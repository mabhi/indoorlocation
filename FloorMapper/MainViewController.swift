//
//  ViewController.swift
//  FloorMapper
//
//  Created by Abhijit Mukherjee on 11/28/15.
//  Copyright © 2015 Tcs. All rights reserved.
//

import UIKit
import CoreLocation

struct SetupInformation{
    
    var setupInfoKeyDistanceFilter: Double
    var setupInfoKeyTimeout : Int64
    var setupInfoKeyHeadingFilter : Double
    var setupInfoKeyAccuracy : CLLocationAccuracy
}

class MainViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var pinLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var pinTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var ibPlanImgView: UIImageView!
    @IBOutlet weak var ibPinImgView: UIImageView!
    @IBOutlet weak var ibContentView: UIView!
    
    @IBOutlet weak var ibBluePin: UIImageView!
    @IBOutlet weak var ibGreenPin: UIImageView!
    
    let locationManager : CLLocationManager
    let anchorPair : GeoAnchorPair
    
    let coordinateConverter : CoordinateConverter
    
    var displayScale : Float = 0.0
    var displayOffset : CGPoint = CGPointZero
    var _bestEffortAtLocation : CLLocation?;

    let setupInfo = SetupInformation(setupInfoKeyDistanceFilter: kCLDistanceFilterNone, setupInfoKeyTimeout: 10,setupInfoKeyHeadingFilter : kCLHeadingFilterNone, setupInfoKeyAccuracy: kCLLocationAccuracyBestForNavigation)
    

    required init?(coder aDecoder: NSCoder) {
        
        locationManager = CLLocationManager ()
//        locationManager.AuthorizationChanged += OnAuthorizationChanged;
//        locationManager.LocationsUpdated += OnLocationsUpdated;
//        locationManager.DesiredAccuracy = CLLocation.AccuracyBest;
//        locationManager.ActivityType = CLActivityType.Other;
        
        // We setup a pair of anchors that will define how the floorplan image, maps to geographic co-ordinates
        
        let anchor1 = GeoAnchor(
            latitudeLongitude : CLLocationCoordinate2D (latitude: 22.583379, longitude: 88.493079),
            pointPixel : CGPointMake (130, 388))
        
        let anchor2 = GeoAnchor (
            latitudeLongitude : CLLocationCoordinate2D (latitude: 22.583388, longitude: 88.4930871),
            pointPixel : CGPointMake (108, 131))
        
        
        /*
//Portrait:Apple sample)
        let anchor1 = GeoAnchor(
            latitudeLongitude : CLLocationCoordinate2D (latitude: 37.770511, longitude: -122.465810),
            pointPixel : CGPointMake (12, 18))
        
        let anchor2 = GeoAnchor (
            latitudeLongitude : CLLocationCoordinate2D (latitude: 37.769125, longitude: -122.466356),
            pointPixel : CGPointMake (481, 815))

        
        let anchor1 = GeoAnchor(
            latitudeLongitude : CLLocationCoordinate2D (latitude: 37.770419, longitude: -122.465726),
            pointPixel : CGPointMake (48, 392))
        
        let anchor2 = GeoAnchor (
            latitudeLongitude : CLLocationCoordinate2D (latitude: 37.769288, longitude: -122.466376),
            pointPixel : CGPointMake (728, 104))
        */
        anchorPair = GeoAnchorPair (fromAnchor: anchor1, toAnchor: anchor2)
        
        // Initialize the coordinate system converter with two anchor points.
        coordinateConverter = CoordinateConverter (anchors: anchorPair);

        super.init(coder: aDecoder)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startTrackingLocation()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func startTracker(sender: AnyObject) {
        if let debugString = setScaleAndOffset(false){
            self.view.makeToast(debugString)
        }
        let sampleLocation1 = CLLocation(latitude: anchorPair.fromAnchor.latitudeLongitude.latitude, longitude: anchorPair.fromAnchor.latitudeLongitude.longitude)
        
        let sampleLocation2 = CLLocation(latitude: anchorPair.toAnchor.latitudeLongitude.latitude, longitude: anchorPair.toAnchor.latitudeLongitude.longitude)

        self.ibBluePin.hidden = false
        dropPinOnMap(self.ibBluePin, location: sampleLocation1)
        self.ibGreenPin.hidden = false
        dropPinOnMap(self.ibGreenPin, location: sampleLocation2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dropPinOnMap(thePin : UIImageView, location : CLLocation){
//        === is identity operator, pointer equality: whether two objects have same reference
        let filteredConstraints = ibContentView.constraints.filter({$0.firstItem as? NSObject === thePin || $0.secondItem as? NSObject === thePin})
        
        let thePointOnMap = self.convertLocationToPoint(location)
        let thePoint = self.ibContentView.convertPoint(thePointOnMap, fromView: self.ibPlanImgView)
        
        
        self.ibContentView.layoutIfNeeded()
        UIView.animateWithDuration(0.25, animations: {[unowned self] in
            let constraint1 = filteredConstraints[0]
            let constraint2 = filteredConstraints[1]
            let imageSize = thePin.image?.size ?? CGSizeZero
            let height = imageSize.height
            let width = imageSize.width
            
              switch constraint1.secondAttribute
            {
                case .Top:
                    constraint1.constant = thePoint.y - height
                case .Leading:
                    constraint1.constant = thePoint.x - width
                default:
                    break
            }
            
            
            switch constraint2.secondAttribute{
                case .Top:
                    constraint2.constant = thePoint.y - height
                case .Leading:
                    constraint1.constant = thePoint.x - width
                default:
                    break
            }
            
            self.ibContentView.layoutIfNeeded()
            
        })
    }
    
    
    func updateViewWithLocation(location : CLLocation){
        
        ibContentView.layoutIfNeeded()
        // We animate transition from one position to the next, this makes the dot move smoothly over the map
        
        UIView.animateWithDuration(0.25, animations: { [unowned self] in
            
            let convertedPoint : CGPoint = self.convertLocationToPoint(location)
//            let scaledPoint = CGPointMake(x, y);
            self.pinLeftConstraint.constant += convertedPoint.x
            self.pinTopConstraint.constant += convertedPoint.y
            self.ibContentView.layoutIfNeeded()
            // Calculate and set the size of the radius
//            let radiusFrameSize = (nfloat)location.HorizontalAccuracy * coordinateConverter.PixelsPerMeter * 2;
//            RadiusView.Frame = new CGRect (0, 0, radiusFrameSize, radiusFrameSize);
            
            // Move the pin and radius to the user's location
//            PinView.Center = scaledPoint;
//            RadiusView.Center = scaledPoint;
        });
    }
    
//      MARK: -
//      MARK: CLLocationManager Delegates
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus){
        switch(status){
            case .AuthorizedAlways,
                .AuthorizedWhenInUse:
                self.view.makeToast("Got authorization, start tracking location")
                startTrackingLocation()
            case .Denied:
                self.view.makeToast("Please authorize location services for this app under Settings > Privacy")
            case .NotDetermined:
                manager.requestWhenInUseAuthorization()
            default:
                break
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError){
//        let message = String(format: "Error code: %d, Reason : %s", error.code,error.localizedDescription)
        let message = "Error code : \(error.code), Reason : \(error.localizedDescription)"
        stopUpdatingLocationWithMessage(message)

    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locationArray = locations as NSArray
        let newLocation = locationArray.lastObject as! CLLocation
        
        let locationAge : NSTimeInterval = -(newLocation.timestamp.timeIntervalSinceNow)
        if (locationAge > 5.0) {
            return;
        }
        
        // test that the horizontal accuracy does not indicate an invalid measurement
        if (newLocation.horizontalAccuracy < 0) {
            return;
        }
        
        
        // test the measurement to see if it is more accurate than the previous measurement
        if(_bestEffortAtLocation?.horizontalAccuracy > newLocation.horizontalAccuracy){
        
            _bestEffortAtLocation = newLocation;
            if(newLocation.horizontalAccuracy <= locationManager.desiredAccuracy){
        // we have a measurement that meets our requirements, so we can stop updating the location
        //
        // IMPORTANT!!! Minimize power usage by stopping the location manager as soon as possible.
            updateViewWithLocation(newLocation)
            self.view.makeToast("Long: \(newLocation.coordinate.longitude), Lat: \(newLocation.coordinate.longitude)")
            }
        
        }
        
//        lblCoordinates.text = String(format: "Long: %.8f, Lat: %.8f", newLocation.coordinate.longitude,newLocation.coordinate.latitude)
    }

//      MARK: -
//      MARK: Location manager start ups
    func startTrackingLocation(){
        let theStatus : CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        if ( CLAuthorizationStatus.NotDetermined == theStatus){
            locationManager.requestWhenInUseAuthorization()
        }
        else if (theStatus == CLAuthorizationStatus.AuthorizedAlways || theStatus == CLAuthorizationStatus.AuthorizedWhenInUse){
            startLocationManager()
        }
    }
    
    func startLocationManager(){
        
        if(CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = setupInfo.setupInfoKeyAccuracy
            locationManager.distanceFilter = setupInfo.setupInfoKeyDistanceFilter
            locationManager.headingFilter = setupInfo.setupInfoKeyHeadingFilter
            locationManager.startUpdatingLocation()
            /*
            let delayInTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * NSEC_PER_SEC))
            dispatch_after(delayInTime, dispatch_get_main_queue(),{
            self.stopUpdatingLocationWithMessage("Timed Out")
            })
            */
        }
        else{
            self.view.makeToast("Location services disabled.")
        }
        
    }
    
    func stopUpdatingLocationWithMessage(message : String) -> Void {
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil
        self.view.makeToast(message)
 
    }
    
//    MARK -
//    MARK : Utility methods
    func convertLocationToPoint(location : CLLocation) -> CGPoint{
        // Call the converter to find these coordinates on our floorplan.
        let pointOnImage = self.coordinateConverter.convert(location.coordinate);
        
        // These coordinates need to be scaled based on how much the image has been scaled
        let x = pointOnImage.x * CGFloat(self.displayScale) + self.displayOffset.x;
        let y = pointOnImage.y * CGFloat(self.displayScale) + self.displayOffset.y;
        return CGPointMake(x, y)
    }
    
    func setScaleAndOffset(showDebug : Bool) -> String?{
        
        var debugString = ""
        let imageViewFrameSize : CGSize = self.ibPlanImgView.frame.size;
        let imageSize : CGSize = self.ibPlanImgView.image!.size
        
        // Calculate how much we'll be scaling the image to fit on screen.
        let wRatio = Float(imageViewFrameSize.width / imageSize.width);
        let hRatio = Float(imageViewFrameSize.height / imageSize.height);
        displayScale = min(wRatio, hRatio);
        debugString += "Scale : \(displayScale)\n"
        
        // Depending on whether we're constrained by width or height,
        // figure out how much our floorplan pixels need to be offset to adjust for the image being centered
        if (wRatio < hRatio) {
            debugString += "Constrained by width\n"
            displayOffset = CGPointMake (0, CGFloat((Float(imageViewFrameSize.height) - Float(imageSize.height) * displayScale) / Float(2)))
        } else {
            debugString += "Constrained by height\n"
            displayOffset = CGPointMake(CGFloat((Float(imageViewFrameSize.width) - Float(imageSize.width) * displayScale) / Float(2)), 0);
        }
        
        debugString += "Offset: x=\(displayOffset.x), y=\(displayOffset.y)"
        
        if(showDebug){
            return debugString
        }
        else{
            return nil;
        }
    }

    
}


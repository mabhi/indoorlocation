//
//  GeoAnchor.swift
//  FloorMapper
//
//  Created by Abhijit Mukherjee on 12/2/15.
//  Copyright © 2015 Tcs. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics

struct GeoAnchor {
    var latitudeLongitude : CLLocationCoordinate2D;
    var pointPixel : CGPoint;
}
//
//  EastSouthDistance.swift
//  FloorMapper
//
//  Created by Abhijit Mukherjee on 12/2/15.
//  Copyright © 2015 Tcs. All rights reserved.
//

import Foundation
import CoreGraphics

// Struct that contains a point in meters (east and south) with respect to an origin point (in geographic space)
// We use East & South because when drawing on an image, origin (0,0) is on the top-left.
// So +eastMeters corresponds to +x and +southMeters corresponds to +y
struct EastSouthDistance {
    var east : Float
    var south : Float
    
    func convertToPoint() -> CGPoint{
        return CGPointMake(CGFloat(east), CGFloat(south))
    }
}
//
//  CoordinateConverter.swift
//  FloorMapper
//
//  Created by Abhijit Mukherjee on 12/2/15.
//  Copyright © 2015 Tcs. All rights reserved.
//

import Foundation
import CoreLocation
//import CoreGraphics
import MapKit


struct GeoAnchorPair {
    var fromAnchor : GeoAnchor
    var toAnchor : GeoAnchor
}

class CoordinateConverter {
    
    // We pick one of the anchors on the floorplan as an origin point that we will compute distance relative to.
    private var anchors: GeoAnchorPair
    private var fromAnchorMKPoint : MKMapPoint
    private var fromAnchorFloorplanPoint : CGPoint
    let pixelsPerMeter : Double
    let radiansRotated : Float
    
    
    /**
     Initializes this class from a given GeoAnchorPair
     
     - parameter Anchors: the anchors that this class will use for converting
     */
    init(anchors : GeoAnchorPair){
        // To compute the distance between two geographical co-ordinates, we first need to
        // convert to MapKit co-ordinates ...
        self.anchors = anchors
        fromAnchorFloorplanPoint = anchors.fromAnchor.pointPixel
        
        fromAnchorMKPoint = MKMapPointForCoordinate(anchors.fromAnchor.latitudeLongitude)
        let toAnchorMKPoint = MKMapPointForCoordinate(anchors.toAnchor.latitudeLongitude)
        
        /*
        ...so that we can use MapKit's Mercator coordinate system where +x
        is always eastward and +y is always southward. 
        This helper function takes into account the curvature of the earth.
        Imagine an arrow connecting fromAnchor to toAnchor...
        
        */
   
        let dx  = anchors.toAnchor.pointPixel.x - anchors.fromAnchor.pointPixel.x
        let dy =  anchors.toAnchor.pointPixel.y - anchors.fromAnchor.pointPixel.y
        
        let distanceBetweenPointsMeter = MKMetersBetweenMapPoints(fromAnchorMKPoint, toAnchorMKPoint)
        
        
        // Distance between two points in pixels (on the floorplan image)
        let distanceBetweenPointsPixels =  Double(hypot(dx, dy))

        // This gives us pixels/meter
        pixelsPerMeter = distanceBetweenPointsPixels / distanceBetweenPointsMeter
        
        // Get the 2nd anchor's eastward/southward distance in meters from the first anchor point.
        let hyp = CoordinateConverter.fetchRect (fromAnchorMKPoint, toAnchorMKPoint);
        
        // Angle of diagonal to east (in geographic)
        let angleFromEastAndHypo = Float(atan2(hyp.south, hyp.east));
        
        // Angle of diagonal to horizontal (in floorplan)
        let angleFromXAndHypo = Float(atan2 (dy, dx));
        
        // Rotation amount from the geographic anchor line segment
        // to the floorplan anchor line segment
        // This is angle between X axis and East direction. This angle shows how you floor plan exists in real world
        radiansRotated = angleFromXAndHypo - angleFromEastAndHypo;

        
    }
    
    // This will anchor image to geographic coordinates
    convenience init(topLeft : CLLocationCoordinate2D, bottomRight : CLLocationCoordinate2D, imageSize : CGSize){
        
        let topLeftAnchor = GeoAnchor (
            latitudeLongitude : topLeft,
            pointPixel : CGPointZero
        );
        
        let bottomRightAnchor = GeoAnchor(
            latitudeLongitude : bottomRight,
            pointPixel : CGPointMake(imageSize.width, imageSize.height)
        );
        
        let geoAnchorPair : GeoAnchorPair = GeoAnchorPair(fromAnchor: topLeftAnchor,toAnchor: bottomRightAnchor)
        self.init(anchors: geoAnchorPair)
    }
    

    // Convert the specified geographic coordinate to floorplan point
    func convert (coordinate : CLLocationCoordinate2D) -> CGPoint
    {
    // Get the distance east & south with respect to the first anchor point in meters
        let rect  = CoordinateConverter.fetchRect (fromAnchorMKPoint, MKMapPointForCoordinate(coordinate))
    
    // Convert the east-south anchor point distance to pixels (still in east-south)
        let scaleTransform = CGAffineTransformMakeScale (CGFloat(pixelsPerMeter), CGFloat( pixelsPerMeter));
        
        let pixelsXYInEastSouth = CGPointApplyAffineTransform(rect.convertToPoint(),scaleTransform);
    
    // Rotate the east-south distance to be relative to floorplan horizontal
    // This gives us an xy distance in pixels from the anchor point.
        let rotateTransform = CGAffineTransformMakeRotation (CGFloat(radiansRotated));
        var xy = CGPointApplyAffineTransform(pixelsXYInEastSouth, rotateTransform)
    
    // From Anchor point may not be top letf corner.
    // however, we need the pixels from the (0, 0) of the floorplan
    // so we adjust by the position of the anchor point in the floorplan
        xy.x += fromAnchorFloorplanPoint.x;
        xy.y += fromAnchorFloorplanPoint.y;
    
        return xy;
    }


    // Two points in rectangular co-ordinate system create a rectange
    static func fetchRect(fromAnchorMKPoint : MKMapPoint,_ toPoint: MKMapPoint) -> EastSouthDistance{
        
        let latitude = MKCoordinateForMapPoint(fromAnchorMKPoint).latitude
        let metersPerPoint = MKMetersPerMapPointAtLatitude(latitude)
        
        let eastSouthDistance = EastSouthDistance(
            east: Float((toPoint.x - fromAnchorMKPoint.x) * metersPerPoint),
            south: Float((toPoint.y - fromAnchorMKPoint.y) * metersPerPoint)
        )
        
        return eastSouthDistance
    }
    
}